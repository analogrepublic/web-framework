# Web Framework

This is a base dependency to be required via composer when writing a frontend/service for a web application. It extracts the core framework components like the `App` class and the `Router` and `Template` systems which previously were found under `app/lib` in `mvc-base`.

Include it in the composer.json like so (Replace `dev-master` with the appropriate version):

```
{
    ...
    "repositories": [{
        "type": "git",
        "url": "https://analogrepublic@bitbucket.org/analogrepublic/web-framework.git"
    }],
    "require": {
        "analogrepublic/web-framework": "dev-master",
        ...
    },
    ...
}
``` 

Be sure to checkout `web-framework-template` [here](https://bitbucket.org/analogrepublic/web-framework-template) to see how it's used.

## Namespacing

Classes under this package are namespaced as `Framework\*`. Be sure to place any classes in the appropriate namespaced folders.

## Versioning

Be sure to use `semver` when versioning changes to this base package as many projects could depend on it, you don't want your changes breaking other projects. Also ensure that projects which depend on this target specific versions.

## Configuration

To ensure this framework core works correctly the following keys MUST be passed into the `$config` param of the `start()` method on the `App` class OR defined as constants using `define()` prior to the first `App::getInstance()` call.

```
    // System options
    'ENABLE_SSL'          => false,
    'ENABLE_MODULE_CACHE' => true,
    'SITE_ENVIRONMENT'    => 'dev',

    // Values & Text
    'SITE_TITLE'          => 'Application',
    'DATE_FORMAT'         => 'm/d/Y',
    'TIME_FORMAT'         => 'h:iA',

    // Directories & Paths
    'PATH_FROM_ROOT'      => DS,
    'URL_BASE'            => DS,
    'FOLDER_ASSETS'       => 'assets',
    'FOLDER_IMG'          => 'img',
    'FOLDER_FILES'        => 'files',
    'URL_ASSETS'          => DIR_ROOT . DS . 'assets' . DS,
    'URL_IMG'             => DIR_ROOT . DS . 'assets' . DS . 'img' . DS,
    'DIR_APP'             => DIR_ROOT . DS . 'app' . DS,
    'DIR_APP_CACHE'       => DIR_ROOT . DS . 'app' . DS . 'cache' . DS,
    'DIR_APP_CONFIG'      => DIR_ROOT . DS . 'app' . DS . 'config' . DS,
    'DIR_APP_VIEWS'       => DIR_ROOT . DS . 'app' . DS . 'views' . DS,

    // Namespaces
    'MODULE_NAMESPACE'     => 'App\\Modules\\',
    'CONTROLLER_NAMESPACE' => 'App\\Controllers\\',
    'MODEL_NAMESPACE'      => 'App\\Models\\',
```

In `web-framework-template` these are defined in `app/config/config.php`, included into the `index.php` and passed to the `start()` method on the `App` instance.

## Helpers & Sugars

### App & Component Instances
To make the code cleaner and simpler for you important components of the app are assigned to the primary `Framework\Lib\App` instance.
 - Router
 - Response
 - Template
 - Request
 - Logger
 - Config

Instead of writing `App::getInstance()` to retrieve the app instance you can now just call `app()`. This means you won't have to ensure you include the full namespace every time you use it, and will make it cleaner when using inline in the views.

Using this function you can also retrieve the components. Example:

Was:
```
$router = Framework\Lib\App::getInstance()->router;
```

Now:
```
$router = app('router');
```

### Route URLs
Previously to generate a url from a route name you'd have to use the full Router namespace `Framework\Lib\Router` and call either `generateUrl` or `url`, you can now call `route_url`.
