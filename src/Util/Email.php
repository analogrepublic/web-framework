<?php

namespace Framework\Util;

class Email
{
    private $to_name;
    private $to_email;
    private $from_name;
    private $from_email;

    private $replyto_name;
    private $replyto_email;

    private $subject;

    private $is_html = true;
    private $is_text = true;

    private $template_name;
    private $template_html;
    private $template_text;

    private $processed_template_html;
    private $processed_template_text;

    private $replacements = [];
    private $attachments = [];

    private $replacement_start = '%%';
    private $replacement_end = '%%';

    public function __construct()
    {
        $this->replacements['URL_IMAGES'] = env('ASSET_CDN_URL') . 'email/';
        $this->replacements['URL_BASE'] = HTTP_URL_BASE;
    }

    public function setToName($to_name)
    {
        $this->to_name = $to_name;
        $this->replacements['TO_NAME'] = $this->to_name;
        return $this;
    }

    public function setToEmail($to_email)
    {
        if (filter_var($to_email, FILTER_VALIDATE_EMAIL)) {
            $this->to_email = $to_email;
            $this->replacements['TO_EMAIL'] = $this->to_email;

            return $this;
        }

        throw new \Exception("Invalid To email address");
    }

    public function setFromName($from_name)
    {
        $this->from_name = $from_name;
        $this->replacements['FROM_NAME'] = $this->from_name;
        return $this;
    }

    public function setFromEmail($from_email)
    {
        if (filter_var($from_email, FILTER_VALIDATE_EMAIL)) {
            $this->from_email = $from_email;
            $this->replacements['FROM_EMAIL'] = $this->from_email;
            return $this;
        }

        throw new \Exception("Invalid From email address");
    }

    public function setReplytoName($replyto_name)
    {
        $this->replyto_name = $replyto_name;
        return $this;
    }

    public function setReplytoEmail($replyto_email)
    {
        if (filter_var($replyto_email, FILTER_VALIDATE_EMAIL)) {
            $this->replyto_email = $replyto_email;
            return $this;
        }

        throw new \Exception("Invalid Reply To email address");
    }

    public function setTemplate($template)
    {
        if ($this->is_html) {
            $html_template = DIR_APP_VIEWS . 'emails/'.$template.'.html';
            if (file_exists($html_template)) {
                $this->template_html = file_get_contents($html_template);
            } else {
                throw new \Exception("HTML template doesn't exists - ".$html_template);
            }
        }

        if ($this->is_text) {
            $text_template = DIR_APP_VIEWS . 'emails/'.$template.'.txt';
            if (file_exists($text_template)) {
                $this->template_text = file_get_contents($text_template);
            } else {
                throw new \Exception("Text template doesn't exists - ".$text_template);
            }
        }

        $this->template_name = $template;
        return $this;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setIsHTML($is_html)
    {
        $this->is_html = (bool)$is_html;
        return $this;
    }

    public function setIsText($is_text)
    {
        $this->is_text = (bool)$is_text;
        return $this;
    }

    public function addAttachment($attachment)
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    public function setReplacements($replacements)
    {
        if (is_array($replacements)) {
            $this->replacements = array_merge($this->replacements, $replacements);
        } else {
            throw new \Exception("Replacements must be array");
        }
        return $this;
    }

    public function addReplacement($key, $value)
    {
        $this->replacements[$key] = $value;
        return $this;
    }

    public function removeReplacement($key)
    {
        unset($this->replacements[$key]);
        return $this;
    }

    private function makeReplacements()
    {
        $this->processed_template_html = $this->template_html;
        $this->processed_template_text = $this->template_text;

        foreach ($this->replacements as $key => $value) {
            if ($this->is_html) {
                $this->processed_template_html = str_replace($this->replacement_start.$key.$this->replacement_end, $value, $this->processed_template_html);
            }
            if ($this->is_text) {
                $this->processed_template_text = str_replace($this->replacement_start.$key.$this->replacement_end, $value, $this->processed_template_text);
            }
        }

        $this->processed_template_html = preg_replace('/('.preg_quote($this->replacement_start).'([^'.preg_quote($this->replacement_end).']+)'.preg_quote($this->replacement_end).')/', '[Replacement for $1 not found]', $this->processed_template_html);
    }

    public function preview()
    {
        $this->makeReplacements();
        if ($this->is_html) {
            return $this->processed_template_html;
        } else {
            return '<pre>'.$this->processed_template_text.'</pre>';
        }
    }

    public function send()
    {
        $this->makeReplacements();

        $sg_key = env('SENDGRID_API_KEY', false);

        if (!$sg_key) {
            throw new \Exception('Send failed');
        }

        $subject = $this->subject;
        $from    = new \SendGrid\Email($this->from_name, $this->from_email);
        $to      = new \SendGrid\Email($this->to_name, $this->to_email);
        $content = new \SendGrid\Content("text/html", $this->processed_template_html);
        $mail    = new \SendGrid\Mail($from, $subject, $to, $content);

        if ($this->replyto_email) {
            $replyto = new \SendGrid\ReplyTo($this->replyto_email, $this->replyto_name);
            $mail->setReplyTo($replyto);
        }

        $sg = new \SendGrid($sg_key);

        foreach ($this->attachments as $attachment_data) {
            if ($attachment_data['type'] == 'url') {
                $file_contents = file_get_contents($attachment_data['url']);
                if (!$file_contents) {
                    continue;
                }
                $file_encoded = base64_encode($file_contents);
                $attachment = new \SendGrid\Attachment();
                $attachment->setContent($file_encoded);
                $attachment->setType($attachment_data['filetype']);
                $attachment->setDisposition('attachment');
                $attachment->setFilename($attachment_data['filename']);
                $mail->addAttachment($attachment);
            }
        }

        $response = $sg->client->mail()->send()->post($mail);

        if ((int)$response->statusCode() <= 202) {
            return true;
        } else {
            throw new \Exception('Send failed, status code: ' . $response->statusCode());
        }
    }
}
