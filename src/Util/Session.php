<?php 

namespace Framework\Util;

/**
 * Wrap around the session and provide some
 * flashing of session dta.
 */
class Session
{

    /**
     * Check if the session contains a
     * specific key
     * 
     * @param  String  $key
     * @return boolean
     */
    public static function has($key)
    {
        return isset($_SESSION['Session::' . $key]);
    }

    /**
     * Set the expirey time of a key
     * 
     * @param  String $key
     * @param  Int    $when
     * @return void
     */
    public static function setExpires($key, $when)
    {
        $current = self::getExpiringData();
        self::set('settings::expires', $current + [$key => $when]);
    }

    /**
     * Get all of the expiring session keys with
     * the times that they expire.
     * 
     * @param  Array  $default
     * @return Array
     */
    public static function getExpiringData($default = [])
    {
        return self::get('settings::expires', $default);
    }

    /**
     * Clean out the expired session data
     * 
     * @return void
     */
    public static function cleanExpired()
    {
        $expires = self::getExpiringData();

        foreach ($expires as $key => $time) {
            if ($time <= time()) {
                unset($expires[$key]);
                self::delete($key);
            }
        }

        self::set('settings::expires', $expires);
    }

    /**
     * Get something from the session
     * 
     * @param  String $key
     * @param  string $fallback
     * @return Mixed          
     */
    public static function get($key, $fallback = false)
    {
        if (!self::has($key)) {
            return $fallback;
        }
        
        $data = $_SESSION['Session::' . $key];

        return $data;
    }

    /**
     * Set some data in the session
     * 
     * @param String $key
     * @param Mixed $value
     */
    public static function set($key, $value, $expires = false)
    {
        $data = ($_SESSION['Session::' . $key] = $value);

        if ($expires && $expires > time()) {
            $at = (int)$expires;
            self::setExpires($key, $at);
        }

        return $data;
    }

    /**
     * Delete some data
     * @param  String $key
     */
    public static function delete($key)
    {
        unset($_SESSION['Session::' . $key]);
    }

    /**
     * Set some flash data
     * 
     * @param  String $key
     * @param  Mixed $value
     * @return Mixed
     */
    public static function flash($key, $value)
    {
        return self::set('flash::' . $key, $value);
    }

    /**
     * Get flash session data by key and destroy
     * 
     * @param  String  $key
     * @param  mixed $fallback
     * @return mixed           
     */
    public static function getFlash($key, $fallback = false)
    {
        $data = self::get('flash::' . $key, $fallback);
        if ($data) {
            self::delete('flash::' .$key);
        }
        return $data;
    }

    /**
     * If we staticly try to access some data
     * with a method call, use get()
     * 
     * @param  String $method
     * @return Mixed
     */
    public static function __callStatic($method, $arguments)
    {
        return self::get($method, $arguments[0]);
    }
}
