<?php 

namespace Framework\Util;

/**
 * Build up a social object to store all
 * of the links to social networks.
 */
class Social
{
    public $facebook = false;
    public $twitter = false;
    public $youtube = false;
    public $googleplus = false;
    public $instagram = false;
    public $linkedin = false;

    /**
     * Gets the value of facebook.
     *
     * @return mixed
     */
    public function getFacebookLink()
    {
        return $this->facebook;
    }

    /**
     * Gets the value of twitter.
     *
     * @return mixed
     */
    public function getTwitterLink()
    {
        return $this->twitter;
    }

    /**
     * Gets the value of youtube.
     *
     * @return mixed
     */
    public function getYoutubeLink()
    {
        return $this->youtube;
    }

    /**
     * Gets the value of googleplus.
     *
     * @return mixed
     */
    public function getGoogleplusLink()
    {
        return $this->googleplus;
    }

    /**
     * Gets the value of instagram.
     *
     * @return mixed
     */
    public function getInstagramLink()
    {
        return $this->instagram;
    }

    /**
     * Sets the value of facebook.
     *
     * @param mixed $facebook the facebook
     *
     * @return self
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Sets the value of twitter.
     *
     * @param mixed $twitter the twitter
     *
     * @return self
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Sets the value of youtube.
     *
     * @param mixed $youtube the youtube
     *
     * @return self
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Sets the value of googleplus.
     *
     * @param mixed $googleplus the googleplus
     *
     * @return self
     */
    public function setGoogleplus($googleplus)
    {
        $this->googleplus = $googleplus;

        return $this;
    }

    /**
     * Sets the value of instagram.
     *
     * @param mixed $instagram the instagram
     *
     * @return self
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Gets the value of linkedin.
     *
     * @return mixed
     */
    public function getLinkedinLink()
    {
        return $this->linkedin;
    }

    /**
     * Sets the value of linkedin.
     *
     * @param mixed $linkedin the linkedin
     *
     * @return self
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }
}
