<?php 

namespace Framework\Util;

use Framework\Lib\App;
use Framework\Util\Session;
use Framework\Util\Token;

/**
 * Handle Cross Site Request Forgery
 */
class CSRF
{
    const TOKEN_FIELD_NAME = 'csrf_token';
    const SESSION_KEY = 'ss::csrf_token';

    public static $index = 0;

    /**
     * Set the index which we point to 
     * 
     * @param integer $index
     */
    public static function setIndex($index = 0)
    {
        self::$index = $index;
    }

    /**
     * Clear the current index
     * 
     * @return void
     */
    public static function clear()
    {
        $session = Session::get(self::SESSION_KEY);

        if (self::$index) {
            unset($session[self::$index]);
            Session::set(self::SESSION_KEY, $session);
        }
    }

    /**
     * Check to see if the CSRF token is valid
     * 
     * @return Boolean
     */
    public static function check()
    {
        $session = Session::get(self::SESSION_KEY);
        $app = App::getInstance();
        
        if ($app->request->has(self::TOKEN_FIELD_NAME)) {
            $token = $app->request->get(self::TOKEN_FIELD_NAME);

            if ($session) {
                self::$index = (array_search($token, $session));
                return self::$index !== false;
            }
        }

        return false;
    }

    /**
     * Grab the token stored in the session
     * 
     * @return string
     */
    public static function getToken()
    {
        $session = Session::get(self::SESSION_KEY);
        end($session);
        $token = current($session);
        return $token;
    }

    /**
     * Get the form field which holds the token
     * 
     * @return String
     */
    public static function getField()
    {
        return '<input type="hidden" name="' . self::TOKEN_FIELD_NAME .'" value="' . self::getToken() . '">';
    }

    /**
     * Generate a new token
     * 
     * @return String
     */
    public static function generate()
    {
        $session = Session::get(self::SESSION_KEY);

        if (!$session) {
            Session::set(self::SESSION_KEY, []);
        }

        if (!is_array($session)) {
            $session = [$session];
            Session::set(self::SESSION_KEY, $session);
        }

        $token = new Token;

        $count = count($session);
        if ($count > 5) {
            $offset = $count - 5;
            $session = array_slice($session, $offset, 5, false);
        }

        $session[] = $token;
        Session::set(self::SESSION_KEY, $session);
        return $token;
    }
}
