<?php 

namespace Framework\Util;

/**
 * Class to represent a token
 */
class Token
{
    private $value = '';

    /**
     * Construct a new token
     */
    public function __construct()
    {
        $this->value = $this->generate();
    }

    /**
     * Generate a new token
     * 
     * @param  string $random
     * @return string
     */
    public function generate($random = false)
    {
        $random = $random ? $random : (string)time();
        return md5(str_shuffle($random));
    }

    /**
     * Handle treating this as a string
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * Get the value which would be
     * the generated token.
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
