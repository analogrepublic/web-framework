<?php

namespace Framework\Util\Validation;

use Framework\Lib\Request;
use Framework\Util\Validation\Validator;

/**
 * The base request validator, this takes a request
 * and passes the allowed input through validation,
 * then provides access to the validator object so
 * you can see if it failed/fetch errors.
 */
class RequestValidator
{

    /**
     * The request object
     *
     * @var Request
     */
    protected $request;

    /**
     * Where the input data is stored.
     *
     * @var array
     */
    protected $input;

    /**
     * The validator object
     *
     * @var Validator
     */
    protected $validator;

    /**
     * The rules for validation
     *
     * @var array
     */
    protected $rules = [];

    /**
     * The fields that we actually
     * want to be sent in the request.
     *
     * @var array
     */
    protected $fields = [];

    /**
     * Hold custom error messages for fields.
     *
     * @var array
     */
    protected $error_messages = [];

    /**
     * Construct this validator object
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->before();

        $this->input = $this->request->get($this->fields);
        $this->validator = Validator::make($this->input, $this->rules);
        $this->validator->setErrorMessages($this->error_messages);
    }

    /**
     * Run the validation
     *
     * @return Validator
     */
    public function run()
    {
        return $this->validator->process();
    }

    /**
     * Get the input for this request
     *
     * @return array
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Do something before validation
     *
     * @return void
     */
    protected function before()
    {
    }
}
