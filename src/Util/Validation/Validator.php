<?php

namespace Framework\Util\Validation;

use GUMP;

/**
 * Validator wrapper around
 * the GUMP class. Provides an
 * easier interface which integrates
 * more neatly with the format
 * of AJAX responses.
 */
class Validator extends GUMP
{
    protected $input = [];
    protected $rules = [];
    protected $formatted_errors = [];
    protected $passed = false;
    protected $error_messages = [];

    private static $instance;

    private static $setup_custom_validators = false;

    /**
     * Make a new validator with some input and some rules
     * you can optionally disable input sanitization.
     *
     * @param  array   $input
     * @param  array   $rules
     * @param  boolean $sanitize
     * @return Validator
     */
    public static function make($input = [], $rules = [], $sanitize = true)
    {
        self::createCustomValidators();

        if (!self::$instance) {
            self::$instance = new Validator();
        }

        self::$instance->input = $input;
        self::$instance->rules = $rules;

        if ($sanitize) {
            self::$instance->input = self::$instance->sanitize(self::$instance->input);
        }

        return self::$instance;
    }

    /**
     * Set custom error messages on this validator
     *
     * @param array $error_messages
     */
    public function setErrorMessages(array $error_messages)
    {
        $this->error_messages = $error_messages;
    }

    /**
     * Grab the input for this validator
     *
     * @return Array
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Grab the rules for this validator
     *
     * @return Array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Did the validation fail?
     *
     * @return Boolean
     */
    public function failed()
    {
        return $this->passed === false;
    }

    /**
     * Process the validation
     *
     * @return Validator
     */
    public function process()
    {
        $this->validation_rules($this->rules);
        $this->passed = ($this->run($this->input) !== false);
        return $this;
    }

    /**
     * Grab the errors returned after running
     * the validator.
     *
     * @return Array
     */
    public function getErrors()
    {
        $this->formatted_errors = $this->get_errors_array();

        foreach ($this->formatted_errors as $key => $value) {
            unset($this->formatted_errors[$key]);
            $this->formatted_errors[str_replace(' ', '_', strtolower($key))] = $value;
        }

        return $this->formatted_errors;
    }

    /**
     * This is where any custom validators are bound
     *
     * @return void
     */
    protected static function createCustomValidators()
    {
        if (self::$setup_custom_validators) {
            return;
        }

        self::add_validator("array_not_empty", function ($field, $input, $param = null) {
            return is_array($input[$field]) && !empty($input[$field]);
        });

        self::add_validator("array", function ($field, $input, $param = null) {
            return is_array($input[$field]);
        });

        self::add_validator("same_as", function ($field, $input, $param = null) {
            // Allow for empty strings
            if ($input[$field] == '') {
                return true;
            }

            return $input[$param] == $input[$field];
        });

        self::add_validator("not_same_as", function ($field, $input, $param = null) {
            // Allow for empty strings
            if ($input[$field] == '') {
                return true;
            }

            return $input[$param] != $input[$field];
        });

        self::add_validator("before_now", function ($field, $input, $param = null) {
            // Allow for empty strings
            if ($input[$field] == '') {
                return true;
            }

            return ($input[$field] < strtotime('now'));
        });

        self::add_validator("min_len_optional", function ($field, $input, $param = null) {
            if ($input[$field] == '' || !isset($input[$field])) {
                return true;
            }

            if (function_exists('mb_strlen')) {
                if (mb_strlen($input[$field]) >= (int)$param) {
                    return true;
                }

                return false;
            }

            if (strlen($input[$field]) >= (int)$param) {
                return true;
            }

            return false;
        });

        self::add_validator("unicode_alpha_space", function ($field, $input, $param = null) {
            return preg_match('/^(\w+|\s+)+$/iu', $input[$field]);
        });

        self::$setup_custom_validators = true;
    }

    /**
     * Process the validation errors and return an array of errors with field names as keys.
     *
     * @param $convert_to_string
     *
     * @return array | null (if empty)
     */
    public function get_errors_array($convert_to_string = null)
    {
        if (empty($this->errors)) {
            return ($convert_to_string) ? null : [];
        }

        $resp = [];

        foreach ($this->errors as $e) {
            $field = ucwords(str_replace(['_', '-'], chr(32), $e['field']));
            $param = $e['param'];

            // Let's fetch explicit field names if they exist
            if (array_key_exists($e['field'], self::$fields)) {
                $field = self::$fields[$e['field']];
            }

            if (!is_string($e['rule'])) {
                $rule = str_replace('validate_', '', $e['rule_name']);

                if (isset($this->error_messages[$e['field']][$rule])) {
                    $resp[$field] = $this->error_messages[$e['field']][$rule];
                    continue;
                }

                $resp[$field] = parent::get_errors_array($convert_to_string)[$field];
                continue;
            }

            $resp[$field] = parent::get_errors_array()[$field];
        }

        return $resp;
    }

    /**
     * Perform data validation against the provided ruleset.
     *
     * @param mixed $input
     * @param array $ruleset
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function validate(array $input, array $ruleset)
    {
        $this->errors = array();

        foreach ($ruleset as $field => $rules) {
            $rules = explode('|', $rules);

            if (in_array('required', $rules) || (isset($input[$field]) && !is_array($input[$field]))) {
                foreach ($rules as $rule) {
                    $method = null;
                    $param = null;

                    // Check if we have rule parameters
                    if (strstr($rule, ',') !== false) {
                        $rule   = explode(',', $rule);
                        $method = 'validate_'.$rule[0];
                        $param  = $rule[1];
                        $rule   = $rule[0];
                    } else {
                        $method = 'validate_'.$rule;
                    }

                    if (is_callable([$this, $method])) {
                        $result = $this->$method(
                            $field, $input, $param
                        );

                        if (is_array($result)) {
                            $this->errors[] = array_merge($result, [
                                'rule_name' => $rule,
                            ]);
                        }
                    } elseif (isset(self::$validation_methods[$rule])) {
                        $result = call_user_func(self::$validation_methods[$rule], $field, $input, $param);

                        if ($result === false) {
                            $this->errors[] = [
                                'field'     => $field,
                                'value'     => $input,
                                'rule'      => self::$validation_methods[$rule],
                                'rule_name' => $rule,
                                'param'     => $param,
                            ];
                        }
                    } else {
                        throw new \Exception("Validator method '$method' does not exist.");
                    }
                }
            }
        }

        return (count($this->errors) > 0) ? $this->errors : true;
    }
}
