<?php

namespace Framework\Exceptions;

/**
 * Exception to be thrown when requested
 * content cannot be found.
 */
class NotFoundException extends \Exception
{
    // no need for body here
}
