<?php

namespace Framework\Exceptions;

/**
 * Exception to be thrown when
 * a constructor is missing an 
 * action for a route.
 */
class ActionException extends \Exception
{
    /**
     * Construct a new exception
     * 
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct('Missing class action - ' . $message);
    }
}
