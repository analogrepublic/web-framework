<?php

namespace Framework\Exceptions;

/**
 * Exception thrown when a controller
 * or class is missing in the application
 * and is required to be instantiated
 * manually.
 */
class ClassException extends \Exception
{
    /**
     * Construct a new exception
     * 
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct('Missing class - ' . $message);
    }
}
