<?php

namespace Framework\Exceptions;

/**
 * Exception to be thrown when the person
 * who makes the request isn't authorized.
 */
class UnauthorizedException extends \Exception
{
    // no need for body here
}
