<?php

namespace Framework\Exceptions;

/**
 * Exception to be thrown when
 * a module view cannot be loaded.
 */
class ModuleViewException extends \Exception
{
    /**
     * Construct a new exception
     * 
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct('Missing module view - ' . $message);
    }
}
