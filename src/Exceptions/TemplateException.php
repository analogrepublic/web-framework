<?php

namespace Framework\Exceptions;

/**
 * Exception to be thrown when a
 * template cannot be loaded.
 */
class TemplateException extends \Exception
{
    /**
     * Construct a new exception
     * 
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct('Missing view template - ' . $message);
    }
}
