<?php

namespace Framework\Lib;

use PDO;
use PDOException;

class Database extends PDO
{
    public static $db;
    /** @var \PDOStatement $last_statement */
    private $last_statement;

    /**
     * Construct a new database connection
     * using the PDO wrapper.
     */
    public function __construct($dsn, $username, $password)
    {
        parent::__construct($dsn, $username, $password);
        $this->setAttribute(PDO::ATTR_PERSISTENT, true);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->exec("SET NAMES ".DB_CHARSET);
    }

    /**
     * This is a singleton so we return
     * and instance of this statically.
     *
     * @return Database
     */
    public static function getInstance()
    {
        if (!isset(self::$db)) {
            $dsn = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME;
            $username = DB_USERNAME;
            $password = DB_PASSWORD;

            self::$db = new Database($dsn, $username, $password);
        }

        return self::$db;
    }

    /**
     * Perform multiple actions on the database
     *
     * @param  string $table
     * @param  array $data
     * @param  string $action
     * @param  string $update_params
     * @return object
     */
    public function perform($table, $data, $action = 'insert', $update_params = '1')
    {
        if ($action == 'insert') {
            $keys = array();
            $values = array();
            $new_data = array();

            foreach ($data as $k => $v) {
                $keys[] = $k;

                $k = str_replace('-', '_', str_replace('`', '', $k));

                if ($v === 'now()') {
                    $values[] = 'now()';
                } elseif (strpos($v, 'GeomFromText') === 0) {
                    $values[] = $v;
                } else {
                    $values[] = ':'.$k;
                    $new_data[':'.$k] = $v;
                }
            }

            try {
                $query = 'INSERT INTO '.$table.' ('.implode(', ', $keys).') VALUES ('.implode(', ', $values).')';
                $stmt = $this->prepare($query);
                $stmt->execute($new_data);
                $this->last_statement = $stmt;
                return $stmt;
            } catch (PDOException $e) {
                throw $e;
            }
        } elseif ($action == 'update') {
            $keys = array();
            $new_data = array();

            foreach ($data as $k => $v) {
                if ($v === 'now()') {
                    $keys[] = $k.' = now()';
                } elseif (strpos($v, 'GeomFromText') === 0) {
                    $keys[] = $k.' = GeomFromText(?)';
                    $new_data[] = substr(str_replace('GeomFromText(', '', $v), 1, -2);
                } else {
                    $keys[] = $k.' = ?';
                    $new_data[] = $v;
                }
            }
            try {
                $query = 'UPDATE '.$table.' SET '.implode(', ', $keys).' WHERE '.$update_params;
                $stmt = $this->prepare($query);
                $stmt->execute($new_data);
                $this->last_statement = $stmt;
                return $stmt;
            } catch (PDOException $e) {
                throw $e;
            }
        } elseif ($action == 'delete') {
            try {
                $query = 'DELETE FROM ' . $table . ' where ' . $data;
                $stmt = $this->query($query);
                $this->last_statement = $stmt;
                return $stmt;
            } catch (PDOException $e) {
                throw $e;
            }
        }
    }

    public function rowCount()
    {
        return $this->last_statement->rowCount();
    }
}
