<?php

namespace Framework\Lib;

/**
 * Handle basic responses for the application
 * to ensure they all conform to the same
 * format.
 */
class Response
{
    /**
     * Hold the status code for the
     * response.
     *
     * @var integer
     */
    private $statusCode = 200;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var boolean
     */
    private $revalidate = false;

    /**
     * @var string
     */
    private $redirect = false;

    /**
     * Convert input to json, set headers and handle errors. You
     * can optionally encode the input data as utf8, this is 
     * going to happen by default but can be disabled.
     *
     * @param  mixed $input
     * @param  boolean $utf8
     * @param  boolean $clean
     * @return String
     */
    public function json($input, $utf8 = true, $clean = true)
    {
        $headers = [
            'Content-Type' => 'application/json'
        ];

        if ($utf8) {
            $headers['Content-Type'] .= '; charset=utf-8';
            utf8_encode_deep($input);
        }

        $json = json_encode($input);

        if (!$json) {
            throw new \Exception('Could encode response data as JSON.');
        }

        $this->output($json, $headers, $clean);
    }

    /**
     * Produce a standard ajax response for
     * ajax calls based on this platform.
     *
     * @param  array   $data
     * @param  array   $errors
     * @param  boolean $revalidate
     * @param  boolean|string $redirect
     * @return String
     */
    public function ajax(array $data = [], array $errors = [], $revalidate = false, $redirect = false)
    {
        $data = array_merge($data, $this->data);
        $errors = array_merge($errors, $this->errors);
        $revalidate = $this->revalidate || $revalidate;

        if (!$redirect) {
            $redirect = $this->redirect;
        }

        try {
            $this->json([
                'data'       => $data,
                'errors'     => $errors,
                'revalidate' => $revalidate,
                'redirect'   => $redirect,
            ]);
        } catch (\Exception $e) {
            $this->setStatusCode(422)->output('Unable to process return data for request.');
        }
    }

    /**
     * Output the data, with the ability to set some headers.
     * Also enable cleaning the output buffer before returning.
     *
     * @param  String $data
     * @param  array $headers
     * @param  boolean $clean
     * @return void
     */
    public function output($data, $headers = [], $clean = false)
    {
        if ($clean) {        
            ob_clean();
        }

        foreach ($headers as $header_name => $header_value) {
            header($header_name . ': ' . $header_value);
        }

        http_response_code($this->getStatusCode());
        die($data);
    }

    /**
     * Gets the value of statusCode.
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set the status code
     *
     * @param int $code
     * @return $this
     */
    public function setStatusCode($code)
    {
        $this->statusCode = $code;
        return $this;
    }

    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function addError($key, $message)
    {
        $this->errors[$key] = $message;
        return $this;
    }

    public function getErrorCount()
    {
        return count($this->errors);
    }

    public function setRevalidate($revalidate)
    {
        $this->revalidate = (bool)$revalidate;
        return $this;
    }

    public function getRevalidate()
    {
        return $this->revalidate;
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }
}
