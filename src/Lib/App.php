<?php

namespace Framework\Lib;

use Framework\Lib\Request;
use Framework\Lib\Response;
use Framework\Lib\Template;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class App
{
    public static $instance;

    public $config = [];
    public $routes = [];

    public $router;
    public $request;
    public $response;
    public $template;

    private $vars = array();

    public static $exceptionHandlers = [];

    public function __construct()
    {
        $this->response = new Response();
        $this->template = new Template();
        $this->request  = new Request();
    }

    public function start($routes = [], $config = [], $router = 'Framework\Lib\Router')
    {
        $this->config = $config;

        foreach ($this->config as $key => $value) {
            if (!defined($key)) {
                define($key, $value);
            }
        }

        try {
            $this->router = new $router($routes);
            $this->template->setRouter($this->router);
            $this->router->parseRoute();
        } catch (\Exception $e) {
            $classname = get_class($e);

            if ($pos = strrpos($classname, '\\')) {
                $classname = substr($classname, $pos + 1);
            }

            if (isset(self::$exceptionHandlers[$classname])) {
                call_user_func(self::$exceptionHandlers[$classname], $e);
                return;
            }

            // Re-throw to bubble
            // the exception up to the
            // implementation
            throw $e;
        }
    }

    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    public function __get($index)
    {
        return $this->vars[$index];
    }

    public function __isset($index)
    {
        return isset($this->vars[$index]);
    }

    public function __unset($index)
    {
        unset($this->vars[$index]);
    }

    public static function redirect($url, $code = 302, $json = false)
    {
        if ($json && isset(self::getInstance()->response)) {
            return self::getInstance()->response->ajax([], [], false, $url);
        }

        header("Location: ".$url, true, $code);
        exit();
    }

    public static function getInstance()
    {
        if (!isset(App::$instance)) {
            App::$instance = new App();
        }

        return App::$instance;
    }

    public static function isAjaxRequest()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    /**
     * Start sessions, allow for passing ini options.
     *
     * @param  array  $ini_options
     * @return string
     */
    public static function sessions($ini_options = [])
    {
        foreach ($ini_options as $key => $value) {
            ini_set('session.' . $key, $value);
        }

        session_start();

        return session_id();
    }

    /**
     * Setup a system logger.
     *
     * @param   string $name
     * @param   string $format
     * @param   array  $levels
     * @return  \Monolog\Logger
     */
    public static function logger($name = 'system', $format = "%channel%.%level_name%: %message% %context% %extra%\n", $levels = ['DEBUG', 'INFO', 'NOTICE', 'WARNING', 'ERROR', 'CRITICAL'])
    {
        // Create the logger
        $logger = new Logger($name);

        // Create the formatter
        $log_formatter = new LineFormatter($format);

        $allowed_levels = Logger::getLevels();
        $loggers = [];

        foreach ($levels as $level) {
            if (isset($allowed_levels[strtoupper($level)])) {
                $loggers[] = $allowed_levels[strtoupper($level)];
            }
        }

        foreach ($loggers as $type) {
            // Now add some handlers
            $handler = new StreamHandler('php://stdout', $type);
            $handler->setFormatter($log_formatter);
            $logger->pushHandler($handler);
        }

        return self::getInstance()->logger = $logger;
    }

    /**
     * Handle pretty printing of system errors,
     * using the whoops handler.
     *
     * @param boolean $api
     * @return \Whoops\Run
     */
    public static function prettyErrors($api = false)
    {
        $whoops = new \Whoops\Run;

        if (!$api && $_SERVER['REQUEST_METHOD'] != 'POST' && !self::isAjaxRequest()) {
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);

            return $whoops->register();
        }

        $whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler);

        return self::getInstance()->whoops = $whoops;
    }

    /**
     * When the application throw an exception,
     * use a defined handler.
     *
     * @param   string  $exceptions
     * @param   closure $callback
     * @return  void
     */
    public static function addExceptionHandler($exceptions, $callback)
    {
        if (!is_array($exceptions)) {
            $exceptions = [$exceptions];
        }

        foreach ($exceptions as $name) {
            self::$exceptionHandlers[$name] = $callback;
        }
    }
}
