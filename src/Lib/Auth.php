<?php 

namespace Framework\Lib;

use Framework\Util\Session;

/**
 * Handle app auth.
 */
class Auth
{

    public static $user = null;

    const USER_SESSION_KEY = 'user_login';

    /**
     * Login the application and pass in some user data
     * which we can serialize in the session.
     * 
     * @param  boolean|mixed $user
     * @return void
     */
    public static function login($user = false)
    {
        $data = time();
        static::$user = $user;

        if ($user) {
            $data = serialize(static::$user);
        }

        Session::set(self::USER_SESSION_KEY, $data);
    }

    /**
     * Does the app have a logged in user?
     * 
     * @return boolean
     */
    public static function loggedIn()
    {
        return Session::has(self::USER_SESSION_KEY);
    }

    /**
     * Grab the user data from the session once and 
     * store it or if forced will re-fetch from session.
     *
     * @param  boolean $force
     * @return mixed
     */
    public static function user($force = false)
    {
        if (!self::loggedIn()) {
            return null;
        }

        if (!static::$user || $force) {
            static::$user = unserialize(Session::get(self::USER_SESSION_KEY));
        }
        
        return static::$user;
    }

    /**
     * Clear the current session.
     * 
     * @return void
     */
    public static function logout()
    {
        Session::delete(self::USER_SESSION_KEY);
        session_destroy();
    }
}