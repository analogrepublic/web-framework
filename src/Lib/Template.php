<?php

namespace Framework\Lib;

use Framework\Exceptions\TemplateException;
use Framework\Lib\Router;
use Framework\Modules\BaseModule;

class Template
{
    private $router;

    private $vars = [];
    private $module_vars = [];

    public function __construct()
    {
        $this->set('required_js', []);
        $this->set('required_css', []);

        $this->set('validator_revalidate', false);
        $this->set('validator_errors', []);
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
        $this->set('current_url', $this->router->getURI());
        $this->set('url_parts', $this->router->getURIParts());
    }

    /**
     * Render the template
     *
     * @param string $template Template filename including file extension
     * @param Array $vars  Variables which can be passed to the view
     * @throws TemplateException If the template could not be found
     * @return void
     **/
    public function render($template, $vars = [])
    {
        $this->vars = array_merge($this->vars, $vars);

        extract($this->vars);
        $template_require = @include(DIR_APP_VIEWS . $template);

        if (!$template_require) {
            throw new TemplateException($template);
        }
    }

    /**
     * Set a variable for use within the template
     *
     * @param string $index Name of the variable to be used within the template
     * @param mixed $value
     * @return Template
     **/
    public function set($index, $value)
    {
        $this->vars[$index] = $value;
        return $this;
    }

    /**
     * @return array Array of all the template variables
     **/
    public function getVars()
    {
        return $this->vars;
    }

    private function getPathType($type, $path, $folder = '')
    {
        switch ($type) {
            case 'root':
                $path = URL_BASE.$path;
                break;

            case 'full':
                $path = $path;
                break;

            case 'relative':
            default:
                $path = URL_ASSETS.$folder.$path;
                break;
        }

        return $path;
    }

    /**
     * Add JS file URL to the template
     *
     * @param string $path Path/URL to the JS file
     * @param string $type Type of Path/URL
     * @return Template Template object
     **/
    public function addRequiredJS($path, $type = 'relative')
    {
        $this->vars['required_js'][] = $this->getPathType($type, $path, 'js/');
        return $this;
    }

    public function getRequiredJS()
    {
        return $this->vars['required_js'];
    }

    /**
     * Add CSS file URL to the template
     *
     * @param string $path Path/URL to the CSS file
     * @param string $type Type of Path/URL
     * @return Template Template object
     **/
    public function addRequiredCSS($path, $type = 'relative')
    {
        $this->vars['required_css'][] = $this->getPathType($type, $path, 'css/');
        return $this;
    }

    public function getRequiredCSS()
    {
        return $this->vars['required_css'];
    }

    /**
     * Add the tiny-mce library
     */
    public function addTinyMCE()
    {
        $this->addRequiredJS('//cdn.tinymce.com/4/tinymce.min.js', 'full');
        return $this;
    }

    /**
     * Add recaptcha custom js link
     */
    public function addRecaptcha()
    {
        $this->addRequiredJS('https://www.google.com/recaptcha/api.js', 'full');
        return $this;
    }

    /**
     * Add the stripe js library
     */
    public function addStripe()
    {
        $this->addRequiredJS('https://js.stripe.com/v2/', 'full');
        return $this;
    }

    /**
     * Add jQuery UI
     *
     * @return  Template
     */
    public function addJQueryUI()
    {
        $this->addRequiredJS('//code.jquery.com/ui/1.11.3/jquery-ui.min.js', 'full');
        return $this;
    }

    /**
     * Add the C3 library and it's dependencies
     *
     * @return  Template
     */
    public function addC3()
    {
        $this->addRequiredCSS('https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css', 'full');
        $this->addRequiredJS('https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.16/d3.min.js', 'full');
        $this->addRequiredJS('https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.js', 'full');
        return $this;
    }

    /**
     * Add Fancybox JS/CSS to the template
     *
     * @return Template Template object
     **/
    public function addFancybox()
    {
        $this->addRequiredCSS('//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css', 'full');
        $this->addRequiredJS('//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js', 'full');
        $this->addRequiredJS('//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js', 'full');
        return $this;
    }

    /**
     * Add Validation JS/CSS to the template
     *
     * @return Template Template object
     **/
    public function addValidation()
    {
        $this->addRequiredCSS('//cdnjs.cloudflare.com/ajax/libs/qtip2/2.1.1/jquery.qtip.min.css', 'full');
        $this->addRequiredJS('//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js', 'full');
        $this->addRequiredJS('//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js', 'full');
        $this->addRequiredJS('//cdnjs.cloudflare.com/ajax/libs/qtip2/2.1.1/jquery.qtip.min.js', 'full');
        return $this;
    }

    /**
     * Set a variable for a specific module
     *
     * @param string $module Name of the module for variable
     * @param string $index Name of the variable to be used in the module
     * @param mixed $value
     * @return Template
     **/
    public function setModuleVar($module, $index, $value)
    {
        if (!isset($this->module_vars[$module])) {
            $this->module_vars[$module] = [];
        }
        $this->module_vars[$module][$index] = $value;
        return $this;
    }

    /**
     * Load a module from within a Template
     *
     * @param string $module_basename Name of the module to load
     * @param mixed $varname,... Unlimited OPTIONAL number of additional variables to be passed to the template
     * @return void
     **/
    public function loadModule($module_basename)
    {
        $arguments = func_get_args();
        $module_name = $module_basename.'Module';

        if (defined('MODULE_NAMESPACE')) {
            $module_name = MODULE_NAMESPACE . $module_name;
        }

        if (class_exists($module_name)) {
            $module_require = true;
            $module = new $module_name($this);
        }

        if (!$module_require) {
            $module = new BaseModule($this);
            $module->setView($module_basename.'.php');
        }

        if (isset($this->module_vars[$module_basename])) {
            foreach ($this->module_vars[$module_basename] as $key => $value) {
                $module->{$key} = $value;
            }
        }

        if (ENABLE_MODULE_CACHE === true && $module->isCacheable()) {
            $cache_time = $module->getCacheTime();
            $cache_key = $module->getCacheKey($arguments);

            $cache_file = DIR_APP_CACHE . 'module/' . $cache_key . '.html';
            mkdir(DIR_APP_CACHE . 'module/', 0777, true);

            $cache_file_time = @filemtime($cache_file);
            if (!$cache_file_time || $_SERVER['REQUEST_TIME'] > ($cache_time + $cache_file_time)) {
                ob_start();
                $this->renderModule($module, $arguments);
                if (!is_production()) {
                    echo "<!-- ".$cache_key." :: Cached at ".date('m/d/Y H:i')." :: Expires at ".date('m/d/Y H:i', $_SERVER['REQUEST_TIME'] + $cache_time)." -->\n";
                }
                $fp = fopen($cache_file, 'w');
                fwrite($fp, ob_get_contents());
                fclose($fp);
                ob_end_flush();
            } else {
                if ($module->getAlwaysProcess()) {
                    $module->setRendering(false);
                    $this->renderModule($module, $arguments);
                }
                include($cache_file);
            }
        } else {
            $module->setRendering(true);
            $this->renderModule($module, $arguments);
        }
        return $this;
    }

    private function renderModule($module, $arguments)
    {
        array_shift($arguments);
        call_user_func_array(array($module, 'process'), $arguments);
    }
}
