<?php

namespace Framework\Lib;

/**
 * Allow access to request data, non-request
 * method specific.
 */
class Request
{

    /**
     * Where the data for the
     * request is stored.
     *
     * @var array
     */
    public $data = [];

    /**
     * Handle constructing a new request class
     * and pass the app router to it.
     *
     * @param array $data
     */
    public function __construct($data = false)
    {
        $this->data = $this->getRequestData();

        if ($data) {
            $this->data = $data;
        }

        if (!is_array($this->data)) {
            $this->data = [$this->data];
        }
    }

    /**
     * Get specific keys
     *
     * @param  boolean $only
     * @return array
     */
    public function get($only = false, $sanitize = false)
    {
        $data = $this->all($sanitize);

        if (!$only) {
            return $data;
        }

        if (!is_array($only)) {
            if (!isset($data[$only])) {
                return false;
            }

            return $data[$only];
        }

        return array_only_keys($data, $only);
    }

    /**
     * Does the request have the key
     * passed to this function?
     *
     * @param  String  $key
     * @return boolean
     */
    public function has($key)
    {
        return $this->get($key) != null;
    }

    /**
     * Get all of request
     *
     * @return array
     */
    public function all($sanitize = false)
    {
        $data = $this->getData();

        if ($sanitize) {
            $data = $this->sanitize($data);
        }

        return $data;
    }

    /**
     * Sanitize the input
     *
     * @param  array  $what
     * @return array
     */
    public function sanitize(array $what)
    {
        $sanitized = [];

        foreach ($what as $key => $item) {
            if (is_array($item) || is_bool($item)) {
                $sanitized[$key] = $item;
                continue;
            }

            $sanitized[$key] = clean_input($item);
        }

        return $sanitized;
    }

    /**
     * Get this requests data.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the data from the request
     *
     * @return array
     */
    private function getRequestData()
    {
        return array_merge($_REQUEST, $this->getXMLData(), $this->getJSONData());
    }

    private function getXMLData()
    {
        if (isset($_SERVER["CONTENT_TYPE"]) && in_array('application/xml', explode(';', $_SERVER["CONTENT_TYPE"]))) {
            $xml_data = simplexml_load_string(
                file_get_contents('php://input'),
                "SimpleXMLElement",
                LIBXML_PARSEHUGE
            );

            if (!is_array($xml_data)) {
                $xml_data = [$xml_data];
            }

            return ['xml_document' => $xml_data];
        }

        return [];
    }

    private function getJSONData()
    {
        if (isset($_SERVER["CONTENT_TYPE"]) && in_array('application/json', explode(';', $_SERVER["CONTENT_TYPE"]))) {
            $json_data = json_decode(file_get_contents('php://input'), true);

            if (!is_array($json_data)) {
                $json_data = [$json_data];
            }

            return $json_data;
        }

        return [];
    }
}
