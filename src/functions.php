<?php

/**
 * Framework wide functions.
 */

if (!function_exists('clean_url')) {
    /**
     *
     * Clean a string for use in URL, with the option to
     * alter what we sanitize and how we handle the url.
     *
     * @param  string  $url
     * @param  boolean $allow_slashes
     * @param  boolean $allow_full_stops
     * @param  boolean $lowercase
     * @return string
     */
    function clean_url($url, $allow_slashes = false, $allow_full_stops = true, $lowercase = true)
    {
        $url = trim($url);

        if ($lowercase) {
            $url = strtolower($url);
        }

        $url = strip_tags(preg_replace('/&([^;]*);/', '', $url));

        $remove_chars  = array( "([\40])" , "([^a-zA-Z0-9-".($allow_full_stops?'\.':'') . ($allow_slashes?'\/':'')."])", "(-{2,})" );
        $replace_with = array("-", "", "-");

        $url = preg_replace($remove_chars, $replace_with, $url);

        return $url;
    }
}

if (!function_exists('dump')) {
    /**
     * Dump something in a pretty manner
     *
     * @param  Mixed $what
     * @return void
     */
    function dump($what)
    {
        echo '<pre>';
        var_dump($what);
        echo '</pre>';
    }
}

if (!function_exists('dd')) {
    /**
     * Dump something and die
     *
     * @param  Mixed $what
     * @return void
     */
    function dd($what)
    {
        dump($what);
        die();
    }
}

if (!function_exists('clean_input')) {
    /**
     * Clean input
     *
     * @param  String $input
     * @param  Boolean/String $strips_tags
     * @param  Boolean $strip_attributes
     * @return String
     */
    function clean_input($input, $strips_tags = true, $strip_attributes = true)
    {
        if (!$strips_tags) {
            return stripslashes($input);
        }

        $output = $input;
        if ($strip_attributes) {
            $output = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i", '<$1$2>', $output);
        }

        $output = strip_tags($output, $strips_tags);

        return stripslashes($output);
    }
}

if (!function_exists('array_only_keys')) {
    /**
     * Only get the values for keys which
     * exist in the array.
     *
     * @param  Array $haystack
     * @param  Array|String $keys
     * @return Array
     */
    function array_only_keys($haystack, $keys)
    {
        return array_filter($haystack, function ($value, $key) use ($keys) {
            if (!is_array($keys)) {
                return $key == $keys;
            }

            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_BOTH);
    }
}

if (!function_exists('env')) {
    /**
     * Get a key from the ENV
     *
     * @param  strin         $key
     * @param  boolean|mixed $fallback
     * @return string
     */
    function env($key, $fallback = false)
    {
        $env = getenv($key);

        if (!$env) {
            $env = $fallback;
        }

        return $env;
    }
}

if (!function_exists('pluralize')) {
    /**
     * Pluralizes a word if quantity is not one.
     *
     * @param int $quantity Number of items
     * @param string $singular Singular form of word
     * @param string $plural Plural form of word; function will attempt to deduce plural form from singular if not provided
     * @return string Pluralized word if quantity is not one, otherwise singular
     */
    function pluralize($quantity, $singular, $plural = null)
    {
        if ($quantity == 1 || !strlen($singular)) {
            return $singular;
        }
        if ($plural !== null) {
            return $plural;
        }

        $last_letter = strtolower($singular[strlen($singular)-1]);

        switch ($last_letter) {
            case 'y':
                return substr($singular, 0, -1).'ies';
            case 's':
                return $singular.'es';
            default:
                return $singular.'s';
        }
    }
}

if (!function_exists('app')) {
    /**
     * Clean way to retrieve the app instance or
     * a property of the app.
     *
     * @return Framework\Lib\App | mixed
     */
    function app($property = false)
    {
        $instance = Framework\Lib\App::getInstance();

        if ($property && isset($instance->$property)) {
            $instance = $instance->$property;
        }

        return $instance;
    }
}

if (!function_exists('route_url')) {
    /**
     * Clean way to get the url for a route
     *
     * @param  string  $name
     * @param  array   $keys
     * @param  array   $values
     * @param  boolean $full
     * @return string
     */
    function route_url($name, $keys = array(), $values = array(), $full = false)
    {
        return Framework\Lib\Router::url($name, $keys, $values, $full);
    }
}

if (!function_exists('db')) {
    /**
     * Get the DB instance in a nice way.
     *
     * @return Framework\Lib\Database
     */
    function db()
    {
        return Framework\Lib\Database::getInstance();
    }
}

if (!function_exists('is_environment')) {

    /**
     * If we're running in the requested environment
     * then this should return true.
     *
     * @param  string $env
     * @return boolean
     */
    function is_environment($env)
    {
        return env('SITE_ENVIRONMENT', 'local') == $env;
    }
}

if (!function_exists('is_production')) {

    /**
     * If we're running in the production environment
     * then this should return true.
     *
     * @return boolean
     */
    function is_production()
    {
        return is_environment('production');
    }
}

if (!function_exists('is_staging')) {

    /**
     * If we're running in the staging environment
     * then this should return true.
     *
     * @return boolean
     */
    function is_staging()
    {
        return is_environment('staging');
    }
}

if (!function_exists('is_local')) {

    /**
     * If we're running in the local environment
     * then this should return true.
     *
     * @return boolean
     */
    function is_local()
    {
        return is_environment('local');
    }
}


if (!function_exists('utf8_encode_deep')) {

    /**
     * Encode nested structures such as arrays and objects.
     * 
     * @param  mixed &$input
     * @return void
     */
    function utf8_encode_deep(&$input)
    {
        // Handle the case where we're inputting
        // an object into this function.
        if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                utf8_encode_deep($input->$var);
            }

            return;
        }

        // Handle a simple string encoding.
        if (is_string($input)) {
            if (function_exists('mb_convert_encoding')) {
                $input = mb_convert_encoding($input, 'UTF-8');
            } else {            
                $input = utf8_encode($input);
            }
            return;
        }

        // Handle the case where the input is an array
        if (is_array($input)) {
            foreach ($input as &$value) {
                utf8_encode_deep($value);
            }

            unset($value);
            return;
        }
    }
}

if (!function_exists('html_entity_decode_deep')) {

    /**
     * A deep/nested implementation of html_entity_decode
     * 
     * @param  string|array|object $input
     * @return string|array|object
     */
    function html_entity_decode_deep($input)
    {
        // Handle the case where we're inputting
        // an object into this function.
        if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                $input->$var = html_entity_decode_deep($input->$var);
            }

            return $input;
        }

        // Handle a simple string encoding.
        if (is_string($input)) {
            return html_entity_decode($input);
        }

        // Handle the case where the input is an array
        if (is_array($input)) {
            foreach ($input as $key => $value) {
                $input[$key] = html_entity_decode_deep($value);
            }

            return $input;
        }

        return $input;
    }
}