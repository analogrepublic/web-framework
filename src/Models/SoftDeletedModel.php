<?php 

namespace Framework\Models;

use Framework\Lib\Database;
use Framework\Models\BaseModel;

/**
 * Implement a soft-deletes system so that
 * we only pull non-deleted entries, and when we go to delete
 * something, we actually just mark the deleted_at field with
 * the current timestamp.
 */
class SoftDeletedModel extends BaseModel
{
    /**
     * A method which allows us to dynamically decide whether or not
     * we can include soft deleted records in our queries.
     *
     * @return boolean
     */
    public static function includeSoftDeletedRecords()
    {
        return false;
    }

    /**
     * Mark the entry as deleted.
     *
     * @param  int $id
     * @return boolean
     */
    public static function delete($id)
    {
        return parent::update($id, [
            'deleted_at' => time(),
        ]);
    }

    /**
     * The count implementation with a deleted_at
     * check merged in.
     *
     * @param  array  $where
     * @param  array  $bound_values
     * @return int
     */
    public static function count($where = [], $bound_values = [])
    {
        if (!static::includeSoftDeletedRecords()) {
            $where = array_merge($where, ['deleted_at IS NULL']);
        }

        return parent::count($where, $bound_values);
    }

    /**
     * A find implementation where we only query
     * records which don't have the deleted_at set.
     *
     * @param  array  $where
     * @param  array  $bound_values
     * @param  string $extra
     * @return array<SoftDeleteModel>
     */
    public static function find($where = [], $bound_values = [], $extra = '')
    {
        if (!static::includeSoftDeletedRecords()) {
            $where = array_merge($where, ['deleted_at IS NULL']);
        }
        
        return parent::find($where, $bound_values, $extra);
    }
}
