<?php

namespace Framework\Models;

use PDO;
use JsonSerializable;
use Framework\Lib\Database;

class BaseModel implements JsonSerializable
{
    public $id = 0;
    public $active = 1;

    public static function build($params, $instance = null)
    {
        if (!$instance) {
            $class = get_called_class();
            $instance = new $class;
        }
        
        foreach ($params as $key => $value) {
            $instance->{$key} = $value;
        }

        return $instance;
    }

    public static function getImageFolder($size, $type = 'partial')
    {
        $class = get_called_class();
        $folder = $class::$image_folder;
        $folder_size = $folder.'-'.$size;
        
        if ($type == 'path') {
            return DIR_ROOT . FOLDER_ASSETS . DS . FOLDER_IMG . DS . $folder_size . DS;
        } elseif ($type == 's3path') {
            return FOLDER_IMG . '/' . $folder_size . '/';
        } elseif (awsEnabled === true) {
            return awsURL . FOLDER_IMG . '/' . $folder_size . '/';
        } elseif ($type == 'full') {
            return HTTP_URL_BASE . FOLDER_ASSETS . '/' . FOLDER_IMG . '/' . $folder_size . '/';
        }

        return URL_BASE . FOLDER_ASSETS . '/' . FOLDER_IMG . '/' . $folder_size . '/';
    }

    public static function getTableName()
    {
        $class = get_called_class();

        // If there's a table_name on the model, try
        // to get that.
        if (isset($class::$table_name)) {
            return $class::$table_name;
        }

        // If not then try to guess from model name
        return pluralize(2, str_replace('Model', '', $class));
    }

    public static function find($where = array(), $bound_values = array(), $extra = '')
    {
        $where = (isset($where[0])?' where '.implode(' and ', $where):'');
        $query = 'select * from '.static::getTableName(). $where.' '.$extra;
        
        $db = Database::getInstance();
        
        if (!empty($bound_values)) {
            $stmt = $db->prepare($query);
            $stmt->execute($bound_values);
        } else {
            $stmt = $db->query($query);
        }
        
        $objects = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $objects[] = static::build($row);
        }

        return $objects;
    }

    public static function count($where = array(), $bound_values = array())
    {
        $where = (isset($where[0])?' where '.implode(' and ', $where):'');
        $query = 'select count(*) from '.static::getTableName(). $where;
        
        $db = Database::getInstance();
        
        if (!empty($bound_values)) {
            $stmt = $db->prepare($query);
            $stmt->execute($bound_values);
        } else {
            $stmt = $db->query($query);
        }

        return $stmt->fetchColumn();
    }


    /**
     * Find a row by it's ID
     *
     * @param  integer $id
     * @param  array   $where
     * @param  array   $bound
     * @return BaseModel
     */
    public static function findById($id = 0, $where = [], $bound = [])
    {
        $where = array_merge([
            'id = :id',
        ], $where);

        $bound = array_merge([
            'id' => $id,
        ], $bound);

        return current(static::find($where, $bound, 'limit 1'));
    }

    /**
     * Create a new version of this
     *
     * @param  array  $data The data to add
     * @return BaseModel
     */
    public static function create($data = array(), $return_created = true)
    {
        $table = static::getTableName();
        $db = Database::getInstance();
        $db->perform($table, $data);

        if (!$return_created) {
            return true;
        }

        return static::findById($db->lastInsertId());
    }

    /**
     * Create a new version of this
     *
     * @param  array  $data The data to add
     * @return BaseModel
     */
    public static function delete($id)
    {
        $table = static::getTableName();
        $db = Database::getInstance();
        $db->query('delete from ' . $table . ' where id = ' . (int)$id.' limit 1');
        return true;
    }

    /**
     * Update this model
     *
     * @param  array  $data The data to update
     * @return BaseModel
     */
    public static function update($id, $data = array())
    {
        $table = static::getTableName();
        $db = Database::getInstance();
        $db->perform($table, $data, 'update', 'id = ' . (int)$id.' limit 1');
        return static::findById($id);
    }

    /**
     * Convert this model to an array
     * of all public fields.
     *
     * @return Array
     */
    public function toArray()
    {
        return call_user_func('get_object_vars', $this);
    }

    /**
     * Default deletable state
     *
     * @return boolean
     */
    public function isDeletable()
    {
        return true;
    }

    /**
     * Get the id for this record
     *
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * Gets the value of active.
     *
     * @return mixed
     */
    public function isActive()
    {
        return (bool)$this->active;
    }

    /**
     * Handle serializing a model to json
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $data = $this->toArray();
        ksort($data);
        return $data;
    }

    /**
     * Save a model
     *
     * @param  boolean|array $data
     * @return BaseModel
     */
    public function save($data = false)
    {
        if (!$data) {
            $data = $this->toArray();
        }

        if ($this->id && !is_null($this->id)) {        
            return static::update($this->getId(), $data);
        }

        return static::create($data);
    }

    /**
     * Remove this models entry
     *
     * @return boolean
     */
    public function remove()
    {
        return static::delete($this->getId());
    }
}
