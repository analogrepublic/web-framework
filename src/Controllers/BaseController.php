<?php

namespace Framework\Controllers;

use Framework\Util\StatusCodes;

abstract class BaseController
{
    /**
     * Bind the app instance to all
     * of the controllers.
     */
    public function __construct()
    {
        $this->app = app();
    }

    /**
     * Validate the request, return an error response
     * if we have any. Otherwise return the validator.
     *
     * @param  string  $classname
     * @param  boolean $ajax
     * @return BaseRequestValidator
     */
    protected function validate($classname, $ajax = false)
    {
        $validator = new $classname(app('request'));

        $runner = $validator->run();

        if ($runner->failed()) {

            $errors = (array)$runner->getErrors();

            // If we're validating an ajax request,
            // return an ajax error response.
            if ($ajax) {
                return app('response')->ajax([], $errors, true);
            }

            // Otherwise return a standard error response.
            return app('response')->setStatusCode(StatusCodes::HTTP_BAD_REQUEST)->json([
                'result' => 'error',
                'data'   => [
                    'errors' => $errors,
                ],
            ]);
        }

        return $validator;
    }
}
